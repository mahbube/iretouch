<?php
_post("subject,message");

$validation=new validation();
$validation->addRule("ایمیل مشتری",$cusEmail,array("mail"=>true));
$validation->addRule("موضوع",$subject,array("req"=>true));
$ok=$validation->run();

$fileName="pic".time();
$m=fileUploader("file",$fileName,"");
if(!$m["success"]==true)
{
    templ::error($m["message"]);
    $ok=false;
}
else
{
    $fileName=$m["fileName"];
}
$filePath=DOC_ROOT . "uploads/";

if($ok)
{
    $message=include DOC_ROOT . "admin/view/sendMail/messageText.php";
    $email=new mailSender();
    $sended=$email->adminSendMail($cusEmail,$subject,$message,$filePath,$fileName);
    if(!$sended)
    {
        templ::error("ایمیل ارسال نشد");
    }
    else 
    {
        templ::success("ایمیل ارسال شد");
        $query="UPDATE `order` SET `orderStatus`='mailed' WHERE `id`=$id ";
        $result=dbQuery($query);
        if($result) templ::success("وضعیت سفارش بروز شد");        
    }
}
else
{
    templ::error($validation->errors);
}

?>