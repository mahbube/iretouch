<?php 
    _post("cusFirstName,cusLastName,cusEmail,cusTel,serviceType,onlyMiss,comment,serviceType",true);
    _null("err");
    $validation = new validation();
    $validation->addRule("نام",$cusFirstName,array("req"=>true,"trim"=>true));    
    $validation->addRule("نام خانوادگی",$cusLastName,array("req"=>true,"trim"=>true));   
    $validation->addRule("ایمیل",$cusEmail,array("req"=>true,"trim"=>true,"mail"=>true)); 
    $validation->addRule("تلفن",$cusTel,array("req"=>true,"trim"=>true,"type"=>"int"));
    $ok=$validation->run();   
    
?>
<?php
$checked=array("1"=>false,"2"=>false,"3"=>false,"4"=>false);
$serviceType=(isPost()) ? $_POST['serviceType'] : 2;
_var("cusFirstName,cusLastName,cusEmail,cusTel,serviceType,onlyMiss,comment,serviceType");
$checked[$serviceType]=true;
$missChecked=($onlyMiss=="on") ? true : false; 

?>
<?php
if($ok)
    {
    	$timeStamp=time();
    	$factorNumber=$timeStamp;

        $now=jdate("His",null,null,null,"en");
        $fileName1="$now-$factorNumber";
        $fileName="$now-$factorNumber";
        
        $y= jdate("Y",null,null,null,"en");$m= jdate("m",null,null,null,"en");$d= jdate("d",null,null,null,"en");
        //prepare uplodig path :  year as $y/ mounth as $m  / day as $d 

        if(!file_exists(DOC_ROOT . "uploads/$y")) 
        {
            mkdir(DOC_ROOT . "uploads/$y");
            // copy index.php to created directory
            copy(DOC_ROOT . "view/index.php" , DOC_ROOT . "uploads/$y/index.php");
        }
        if(!file_exists(DOC_ROOT . "uploads/$y/$m")) 
        {
            mkdir(DOC_ROOT . "uploads/$y/$m");
            copy(DOC_ROOT . "view/index.php" , DOC_ROOT . "uploads/$y/$m/index.php");
        }
        if(!file_exists(DOC_ROOT . "uploads/$y/$m/$d")) 
        {
            mkdir(DOC_ROOT . "uploads/$y/$m/$d");
            copy(DOC_ROOT . "view/index.php" , DOC_ROOT . "uploads/$y/$m/$d/index.php");
        }
        //upload file
        $cf=chkFile("file") ;
        if($cf["success"]){
	        $f=fileUploader("file",$fileName,"$y/$m/$d");
	        if($f["success"])
	        {
	        	$okUp=true ;
	            $fileName=$f['fileName'];
	        }
	        else
	        {
	            $okUp=false;
	            templ::error($f['message']);
	        }
	    }else
        {
            $okUp=false;
            templ::error($f['message']);
        }
    }
?>

<script type="text/javascript">


    $(document).ready(function(){
       
        $("#step1").fadeIn(300);
        $("#submitForm").click(function(){
                
                if(validate.check())
                {
                    $("#newOrderForm").submit();
                    $("#step2").fadeOut(50,function(){$("#step3").fadeIn(300);});  
                }
                //       
            
        });
        $("a.next").click(function(){
            var target="#"+$(this).data("target");
            var curunt="#"+$(this).data("curunt");
            $(curunt).fadeOut(50,function(){
                $(target).fadeIn(300);    
            });           
        });        
    });

</script>

<div class="newOrder">
	از صحت اطلاعات وارد شده اطمینان دارید؟
    <form id="newOrderForm" action="?act=register" method="post" enctype="multipart/form-data">
    <!-- start step1 --> 
          
	    <div class="inside-content order">
	        <div class="services">
	            <label id="service"><span class="font-face">انتخاب سرویس :</span></label>
	            <label class="radio_btn"><span class="font-face">1- روتوش ساده: (5000 تومان)</span>
	             	<input type="radio" name="serviceType" <?php echo ($checked["1"]) ? "checked=\"checked\" " : "" ?> value="1" /></label>
	            <label class="radio_btn"><span class="font-face">2 - روتوش متوسط : ( 10000 تومان )</span>
	             	<input type="radio" name="serviceType" <?php echo ($checked["2"]) ? "checked=\"checked\" " : "" ?> value="2" /></label>
	            <label class="radio_btn"><span class="font-face">3 - روتوش حرفه ای : ( 15000 تومان )</span>
	            	<input type="radio" name="serviceType" <?php echo ($checked["3"]) ? "checked=\"checked\" " : "" ?> value="3" /></label>
	            <label class="radio_btn"><span class="font-face">4 - روتوش فانتزی : ( 20000 تومان )</span>
	            	<input type="radio" name="serviceType"<?php echo ($checked["4"]) ? "checked=\"checked\" " : "" ?> value="4" /></label>
	        </div><br/><br/><br/><br/><br/><br/>	            
	        <label id="msg"><textarea name="comment" cols="" rows="" placeholder="توضیحات تکمیلی مربوط به سفارش"><?php echo $comment; ?></textarea></label>			
	    </div>
           
        
        <!-- /step1 -->
        
        <!-- step2 -->
        
        
        <div class="inside-content order2">
            <label>
                <span class="font-face">نام :</span>
                <input name="cusFirstName" id="cusFirstName" value="<?php echo $cusFirstName ?>" type="text">
            </label>
            <label>
                <span class="font-face">نام خانوادگی :</span>
                <input name="cusLastName" id="cusLastName" value="<?php echo $cusLastName ?>" type="text">
            </label>
            <label>
                <span class="font-face">ایمیل :</span>
                <input id="cusEmail" type="text" value="<?php echo $cusEmail ?>" name="cusEmail">
            </label>
             <label>
                <span class="font-face">تلفن :</span>
                <input name="cusTel" alt="num" id="cusTel" value="<?php echo $cusTel ?>" type="text">
            </label>
            <?php
            if($okUp){
            	$url="./uploads/$fileName";
            ?>
            <label >
            	<a href="<?php echo $url ; ?>">مشاهده فایل</a>
            </label>


			<?php
            }else{
            	echo "  در مرحله قبل فایل شما با موفقیت آپلود نشد. لطفا مجددا فایل خود را آپلود کنید." ;
            }

            ?>
             <label>
                <span class="font-face">فایل :</span>
                <input name="file" id="file" type="file" />
                <input type="hidden" name="fileName" value="<?php echo $fileName1 ?>" />
                <input type="hidden" name="factorNumber" value="<?php echo $factorNumber ?>" />
                <input type="hidden" name="timeStamp" value="<?php echo $timeStamp ?>" />
                <input type="hidden" name="okUp" value="<?php echo $okUp ?>" />
            </label>             
             <label id="women">
                <span class="font-face">ویرایش فقط توسط بانو انجام شود</span>
                <input name="onlyMiss" <?php echo ($missChecked) ? "checked=\"checked\"" : "" ?> type="checkbox"></label>
             <a id="submitForm" class="next font-face">ارسال</a>
                     		  
            <div class="clear"></div>
        </div>        
        
        <!-- /step2 -->


        <!-- step3 -->
        <div class="step" id="step3">
                <div class="inside-content order2" style="padding-top: 250px">
                   <div class="font-face" style="text-align: center; font-size: 17px; ">لطفا صبر کنید ... </div>  		  
                   <div class="clear"></div>
                </div>        
        </div>
        <!-- /step3 -->
               
    </form>
</div>
