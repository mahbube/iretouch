<?php
  
class arrays
{
    public static $fishStatus = array(
        "waiting"   => "در انتظار تایید",
        "verified"  => "تایید شده",
        "unVerified"=> "تایید نشده"
    );    
    
    public static $orderStatus = array(
        "recieved"  => "دریافت شده",
        "new"       => "جدید",
        "working"   => "در دست اقدام",
        "mailed"    => "ارسال شده است",
        "finished"  => "انجام شده"
    );
	
     public static $serviceType = array(
        ""   => "نامشخص",
        "1"   => "روتوش ساده",
        "2"   => "روتوش متوسط",
        "3"   => "روتوش حرفه ای",
        "4"   => "روتوش فانتزی",
    );
    
     public static $serviceAmount = array(
        ""  => "",
        "1"   => "5000",
        "2"   => "10000",
        "3"   => "15000",
        "4"   => "20000",
    );
     public static $serviceAmountRial = array(
        ""  => "",
        "1"   => "50000",
        "2"   => "100000",
        "3"   => "150000",
        "4"   => "200000",
    );
    
	public static $monthes = array(
		"1"		=>	"فروردین",
		"2"		=>	"اردیبهشت",
        "3"		=>	"خرداد",
        "4"		=>	"تیر",
        "5"		=>	"مرداد",
        "6"		=>	"شهریور",
        "7"		=>	"مهر",
        "8"		=>	"آبان",
        "9"		=>	"آذر",
        "10"    =>	"دی",
        "11"	=>	"بهمن",
        "12"	=>	"اسفند"
	);
	
}

?>