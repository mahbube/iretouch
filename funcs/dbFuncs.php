<?php

function dbCon($host,$userName,$passWord,$dbName)
{
  $con=@mysql_connect($host,$userName,$passWord);
  if (!$con)
    return false;
  $db=@mysql_select_db($dbName);
  if (!$db)
    return false;
  return true;
}

function calcSums($table,$fields,$where="",$groupBy="")
{
    $fields=explode(",",$fields);
    $fieldsS="";
    $cama="";
    foreach($fields as $field)
    {
        $fieldsS=" $cama sum($field) as sum_$field ";
        $cama=",";
    }
    $query="select $fieldsS from $table where $where ";
    $result=dbQuery($query);
    $row=mysql_fetch_array($result);
    if(!$row["sum_$field"]) $row["sum_$field"]=0;    
    return $row;
    
}

function dbQuery($sql)
{
	//mysql_query("SET CHARSET UTF8");
	debugMessage::resetTimer();
	$result=mysql_query($sql);
	new debugQuery( $sql);
	if(!$result)
	{
		debug::add("error", mysql_error());
	}
	return $result;
}
// return insert sql parameter 
function insertSql($insert)
{
	$cama=null;
	$fields=null;
	$values=null;
	foreach($insert as $key => $value)
	{
		$fields.=$cama."`".$key."`";
		$values.=$cama.$value;
		$cama=",";
	}
	
	$sql=" ( ".$fields." ) values ( ".$values." ) ";
	return $sql;
}

//return update sql parameter 
function updateSql($updates)
{
	$cama=null;
	$sql=null;

	foreach($updates as $key => $value)
	{
		$sql.=$cama."`".$key."`"."=".$value;
		$cama=",";
	}
	
	return $sql;
}


?>