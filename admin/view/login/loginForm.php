<!DOCTYPE html PUBLIC "-//W3C//DTD Xtempl 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fa" lang="fa" dir="rtl">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<link rel="shortcut icon" href="<?php echo URL_ROOT ?>resources/image/icon.png" />
		<title>ورود </title>
		
		<?php
			templ::writeResources();
		?>
     <style>
        .login
        {            
            background: none repeat scroll 0 0 #FFFFFF;
			border: 11px solid #BEBCBC;
			height: 220px;
			margin: 150px auto;
			min-height: 150px;
			position: relative;
			width: 601px;
            
        }
        .login p{
            display: block;
            height: 50px;
            margin: 0 auto 20px;
            width: 415px;
        }
        .login p span label{
            display: inline-block;
			float: right;
			font-size: 14px;
			width: 125px;
        }
        .login p span input{
             background: url("http://iretouchpro.com/resources/image/bg-input.png") repeat-x scroll 0 0 transparent;
			 border: 1px solid #989797;
			 border-radius: 7px 7px 7px 7px;
			 color: #666666;
			 float: right;
			 font: 12px tahoma;
			 height: 32px;
			 padding: 0 5px;
			 width: 256px;
        }
        .login .btn{
            background: url("http://iretouchpro.com/resources/image/bg-next2.png") no-repeat scroll center top transparent;
			color: #817F81;
			display: block;
			float: left;
			font-size: 18px;
			height: 31px;
			text-align: center;
			width: 100px;
            margin-left: 22px;            
        }
		.login .btn:hover{
		background: url("http://iretouchpro.com/resources/image/bg-next2.png") no-repeat scroll center bottom transparent;
		color: #FFFFFF;
		text-shadow: 1px 1px #666666;
		}
        .loginError{
            color: red;
            text-align: center;
            padding: 5px 0px 0px;
            
        }
     </style>   
	</head>
	<body style="background-image: none;">
		<script>
			$(document).ready(function(){
				
			})
		</script>
		<div class="wrapper login">
                <div class='loginError'>
            
            <?php    				
                    if (self::$error) {
    					echo self::$error;
    				}
    				?>
                </div>
            <div class="">
    			<form class="form" action="" method="post" class="form" >
    				
    				<p>
    					<span> <label class="font-face">نام کاربری : </label>
    						<input type="text" name="userName" />
    					</span>
    				</p>
    				<p>
    					<span> <label class="font-face">رمز عبور : </label>
    						<input type="password" name="password" />
    					</span>
    				</p>
    				<p>
    					<input type="submit" class="btn font-face" value="ورود" />
    
    				</p>
    			</form>
    		</div>
        </div>
	</body>
</html>
