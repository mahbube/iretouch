<?php

	// 
	//  debug.php
	//  Orders Management System
	//  
	//  Created by HSY on 2012-03-14.
	//  Copyright 2012 HSY. All rights reserved.
	// 
	
	
	class debug
	{
		private static $d = array();
		private static $mt = 0;
		public static function startTimer()
		{
			self::$mt = microtime(true);
		}
		public static function add($type,$text)
		{
			self::$d[]=array("text"=>$text,"type"=>$type,"time"=>(self::$mt != 0 ? round((microtime(true)-self::$mt)*1000,3) : "0"),"trace"=>function_exists("debug_backtrace")?debug_backtrace():array());
			self::$mt=0;
		}
		
		public static function printDebug()
		{
			 if(count(self::$d)<1)
				 return false;
            ?>
            <script>
				$(document).ready(function(){
					$("#debug table").dataTable({"bSortClasses": false, "asStripeClasses": [],"iDisplayLength": 20});
					$("#debug tbody tr").click(function()
                    {
                        $(this).children("td").children(".more_root").modal({minWidth:1000});
                    });
				});
			</script>
            <?php
			echo "<div class='debug' id='debug'><table  dir='ltr'>
			<thead><tr><th style='width:50px'>#</th><th style='width:50px'>Type</th><th style='width:50px'>Time(ms)</th><th>Text</th></tr></thead><tbody>";
			foreach (self::$d as $key => $value) {
				echo "<tr class='".$value['type']."'>
					<td>".$key."</td>
					<td>".$value['type']."</td>
					<td>".$value['time']."</td>
					<td>";
				if(is_array($value['text']))
					var_dump($value['text']);
				else {
					echo $value['text'];
				}
				echo "<div class='more_root hidden' style='text-align:left;direction:ltr'>";
                self::debugPrintCallingFunction($value["trace"]);
                echo "</div></td></tr>";
			}
			echo "</tbody></table></div>";
		}
		public static function returnArray(){
			return self::$d;
		} 
        private static function debugPrintCallingFunction ($bt) 
        { 
            //$bt = debug_backtrace();
              
            for($i = 0; $i <= count($bt) - 1; $i++)
            {
                if(!isset($bt[$i]["file"]))
                    echo("[PHP core called function]<br />");
                else
                    echo("File: ".$bt[$i]["file"]."<br />");
                
                if(isset($bt[$i]["line"]))
                    echo("&nbsp;&nbsp;&nbsp;&nbsp;line ".$bt[$i]["line"]."<br />");
                echo("&nbsp;&nbsp;&nbsp;&nbsp;function called: ".$bt[$i]["function"]);
                
                if($bt[$i]["args"])
                {
                    echo("<br />&nbsp;&nbsp;&nbsp;&nbsp;args: ");
                    for($j = 0; $j <= count($bt[$i]["args"]) - 1; $j++)
                    {
                        if(is_array($bt[$i]["args"][$j]))
                        {
                            print_r($bt[$i]["args"][$j]);
                        }
                        else
                            echo($bt[$i]["args"][$j]);    
                                    
                        if($j != count($bt[$i]["args"]) - 1)
                            echo(", ");
                    }
                }
                echo("<br /><hr />");
            }

        } 

		
	}
	class debugMessage
	{
		function __construct($msg,$type = ""){
			$type = $type != "" ? $type : substr(strtolower(get_class($this)),5);
			if(defined("ISAJAX"))
			{
				
			}
			debug::add($type,$msg);
		}
		public static function resetTimer()
		{
			debug::startTimer();
		}
	}
	class debugLog 		extends debugMessage{}
	class debugWarning 	extends debugMessage{}
	class debugError 	extends debugMessage{}
	class debugSuccess 	extends debugMessage{}
	class debugQuery 	extends debugMessage{}
	

?>