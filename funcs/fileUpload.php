<?php
function chkFile($file){
    $return=array(
        "success"   =>  false,
        "message"   =>  ""
    );
    if($_FILES["$file"]["size"]==0)
    {
        $return["message"]="فایل انتخاب نشده است";
        return $return;
    }else{
        $return['success']=true;
        return $return;
    }  
}
function fileUploader($file,$fileName,$dir)
{   
    $return=array(
        "success"   =>  false,
        "message"   =>  "",
        "fileName"  =>  ""
    );    
    
    // if($_FILES["$file"]["size"]==0)
    // {
    //     $return["message"]="فایل انتخاب نشده است";
    //     return $return;
    // }
    
    $allowedExts = array("jpg", "jpeg", "gif", "png");
    $extension = getFileExtension($_FILES["$file"]["name"]);
    $fileType=$_FILES["$file"]["type"];
    
    $con_type=(
        (strpos($fileType,'image/')===0)
        ||($fileType == "application/zip")
        ||($fileType == "application/x-zip-compressed")
        ||($fileType == "multipart/x-zip")
        ||($fileType == "application/x-compressed")
        ||($fileType == "application/octet-stream")
        );
   // echo $con_type ;exit();
    if ($con_type)
    {
        if ($_FILES[$file]["error"] > 0)
        {
            $return['success']=false;
            $return['message']=$_FILES[$file]["error"];
        }
        else
        {
            
            if(!move_uploaded_file($_FILES["$file"]['tmp_name'],DOC_ROOT."uploads/$dir/$fileName.$extension"))
            {
                $return['success']=false;
                $return['message']="آپلود نشد";   
            }
            else
            {
                $return['success']=true;
                $return['fileName']=$dir."/".$fileName.".".$extension ;
            }
        }
    }
    else
    {
        $return["success"]=false;
        $return["message"]="فایل شما قابل بارگذاری نمی باشد";
    }
    return $return;
}

function getFileExtension($fn)
{
  $f=explode(".",$fn);
  if(is_array($f))
  {
    if(count($f)==2)
    {
      return $f[1];
    }
    else
      return false;
  }
  else
    return false;
}


?>