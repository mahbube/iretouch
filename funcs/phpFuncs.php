<?php

function isPost()
{
	return !empty($_POST);
}

function isGet()
{
	return !empty($_GET);
}

function farsiNum($str)
{
	return tr_num(number_format($str),"fa");
}


function _get($reqs,$escapeString=false,$trim=false)
{
  /** yes **/
  $array = explode(",", $reqs);
  foreach ($array as $req)
  {
    $GLOBALS[$req] = isset($_GET[$req]) ? $_GET[$req] : null;
  	if ($trim) $GLOBALS[$req]=trim($GLOBALS[$req]); 
    if ($escapeString) $GLOBALS[$req]=mysql_real_escape_string($GLOBALS[$req]); 
  }
}


function _post($reqs,$trim=false)
{
  $array = explode(",", $reqs);
  foreach ($array as $req)
  {
    $GLOBALS[$req] = isset($_POST[$req]) ? $_POST[$req] : null;
	if ($trim) $GLOBALS[$req]=trim($GLOBALS[$req]);
  }
}


// if not set post return true
function _rPost($reqs)
{
  $array = explode(",", $reqs);
  foreach ($array as $req)
  {
    $GLOBALS[$req] = isset($_POST[$req]) ? $_POST[$req] : true;
  }
}

// define null vars if not definded
function _var($vars)
{
  $array = explode(",", $vars);
  foreach ($array as $var)
  {
    if (!isset($GLOBALS[$var]))
      $GLOBALS[$var] = null;
  }
}

//define null vars
function _null($vars)
{
  $array = explode(",", $vars);
  foreach ($array as $var)
  {
    $GLOBALS[$var] = null;
  }
}
 

//for integer valuse if value = null then value=0
function zero($val)
{
  if (!$val)
  {
    return 0;
  }
  else
  {
    return $val;
  }
}


//define each field of row as a global var
function fetchRow($row)
{
  foreach ($row as $key => $value)
  {
    $GLOBALS[$key] = $value;
  }
}


//array to string set for sql command
function arrayToSet($array)
{
  if (is_array($array))
  {
    $cama = null;
    $set = null;
    foreach ($array as $value)
    {
      $set .= $cama . $value;
      $cama = ",";
    }
    $set = "($set)";
	return $set;
  }
}

//convert an object to array
function objectToArray($d) {
		if (is_object($d)) {
			// Gets the properties of the given object
			// with get_object_vars function
			$d = get_object_vars($d);
		}
 
		if (is_array($d)) {
			/*
			* Return array converted to object
			* Using __FUNCTION__ (Magic constant)
			* for recursive call
			*/
			return array_map(__FUNCTION__, $d);
		}
		else {
			// Return array
			return $d;
		}
	}


?>