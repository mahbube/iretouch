<?php
class htmlFactory
{

    static function _select($name, $array, $cur, $extra = null)
    {
        echo "<select name=\"$name\" $extra >\n";
        foreach ($array as $key => $value) {
            $selected = null;
            if ($cur == $key)
                $selected = "selected='selected'";
            echo "<option value='$key' $selected>$value</option>";
        }
        echo "\n</select>";
    }
	
	


    static function _checkbox($name, $checked, $extra = "class='check'")
    {
        echo "<input type=\"checkbox\" name=\"$name\"";
        echo $checked ? "checked=\"checked\"" : "";
        echo " " . $extra . " ";
        echo " />";
    }
    static function _text($name, $value = "", $extra = null)
    {
        echo "<input type=\"text\" name=\"$name\"";
        echo trim($value) ? "value=\"$value\"" : "";
        echo $extra;
        echo " />";
    }
	
	
    static function _textarea($name, $value = "", $extra = null)
    {
        echo "<textarea type=\"text\" name=\"$name\" $extra>";
        echo trim($value) ? $value : "";        
        echo " </textarea>";
    }
	
    static function _hidden($name, $value, $extra = null)
    {
        echo " <input type=\"hidden\" name=\"$name\"";
        echo trim($value) ? " value=\"$value\"" : "";
        echo $extra;
        echo " />";
    }

    static function _submit($value, $class = "btn", $extra = null, $name = null,$other = null)
    {
        echo "<p style='text-align:left'><input type=\"submit\" ";

        echo trim($name) ? " name=\"$name\" " : "";
        echo trim($value) ? " value=\"$value\" " : "";
        echo trim($class) ? " class=\"$class\" " : "";
        echo " $extra ";
        echo " />$other<p>";
    }
    static function _button($value, $class = "", $extra = null, $name = null)
    {
        echo sprintf("<button name='%s' class='%s' %s>%s</button>", 
            $name,
            $class,
            $extra,
            $value
        );
    }
    static function _imgButton($value, $class = "", $extra = null, $name = null,$size = 16)
    {
        echo sprintf("<button name='%s' class='trans %s' %s><img src='%s' width='%d' /></button>", 
            $name,
            $class,
            $extra,
            URL_ROOT . "resources/img/" . $value,
            $size
        );
    }
    static function _password($name, $value, $extra = null)
    {
        echo "<input type=\"password\" name=\"$name\"";
        echo trim($value) ? "value=\"$value\"" : "";
        echo $extra;
        echo " />";
    }
    
    static function _list($array)
    {
        $l = "<ul>";
        foreach($array as $i)
        {
            $l .= "<li>$i</li>";
        }
        $l .= "</ul>";
        return $l;
    }

    public static function selectCustomer($name,$value,$value2 = "")
    {
        self::_text();
    }
    
    public static function colorSpan($text,$color)
    {
        echo "<span style='color: $color'>$text</span>";
    }
    
    /**
     * Table Generation
     * 
     * $head as Array e.g. array('id','title','author')
     * $body as Array e.g. array(array('2','Hello world!','Emad Ghasemi','o'=>array())),...)
     * $sort as array e.g. array('title','asc')
     */
    public static function tableGen($body, $head = array(),$sort = array(),$_tagOpt = array())
    {
    	//c class
    	//o other
        $tagOpt = array(
            "table" => array("c"=>"table","o"=>""),
            "thead" => array("c"=>"thead","o"=>""),
            "th"    => array("c"=>"th","o"=>""),
            "tbody" => array("c"=>"tbody","o"=>""),
            "tr"    => array("c"=>"tr","o"=>""),
            "td"    => array("c"=>"td","o"=>""),
        );
        $tagOpt = array_merge($tagOpt,$_tagOpt);
        if(empty($body))
        {
            echo "No Result";
            return;
        }
        $res = "<table class='".$tagOpt["table"]["c"]."' padding='0' margin='0'>";
        
        $_head = '';
        if(!empty($head))
        {
            $_head = "<thead class='".$tagOpt["thead"]["c"]."' ><tr>";
            //for($i = 0; $th = $head; $i++)
            foreach($head as $th)
            {
                $c = "";
                if($sort[0] == $th/* || $sort == $i*/) $c = " sort $sort[1]";
                $_head .= "<th class='".$tagOpt["th"]["c"] ." $c'>$th</th>";
                //$_head .= sprintf( "<th class='%s %s'>%s</th>",$tagOpt["th"]["c"],$c,$th);
            }
            $_head .= "</tr></thead>";
        }
        
        $_body = "<tbody class='".$tagOpt["tbody"]["c"]."' >";
        foreach($body as $item)
        {
            
            $trO = array_merge($tagOpt["tr"],isset($item["other"])?$item["other"]:array());
            $tr = "<tr class='".$trO["c"]."' ".$trO['o'].">";
            new debugQuery ($item);
            foreach($item as $i)
            {
                if(is_array( $i ))continue;
                $tr .= "<td class='".$tagOpt["td"]["c"]."'>$i</td>";
            }
            $tr .= "</tr>";
            $_body .= $tr;
        }
		$_body	.="</tbody>";
        $res	.= $_head . $_body;
        $res	.= "</table>";
        return $res;
    }
    
    
}
?>