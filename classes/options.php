<?php

	// 
	//  options.php
	//  Orders Management System
	//  
	//  Created by HSY on 2012-03-14.
	//  Copyright 2012 HSY. All rights reserved.
	// 
	
	
	class options
	{
		static private $loadedData = array();
		
		public static function get($name,$key,$insertIfNotExist = false,$def = null)
		{
			if(isset($loadedData[$name][$key]))
				return $loadedData[$name][$key];

			$sql 	= "SELECT `data` FROM `options` where `name`='$name' AND `key`='$key'" ;
			$result=dbQuery($sql);
			if(mysql_num_rows($result)==0)
			{
				if($insertIfNotExist)
					return self::set($name,$key,$def);
				else {
					return null;
				}
			}
			$data = mysql_fetch_array($result);
			$data 	= json_decode($data["data"]);
			self::$loadedData[$name][$key] = $data;
			//new debugLog($data);
			return $data;
		}
		public static function set($name,$key,$data)
		{
			$data = json_encode($data,JSON_UNESCAPED_UNICODE);
			$sqlInsert 	= "INSERT INTO `options` values ('$name','$key','$data');";
			$sqlUpdate 	= "UPDATE `options` set `data`='$data' WHERE `name`='$name' " . ($key ? "AND `key`='$key'" : "");
			
			$result	= dbQuery("SELECT `data` FROM `options` where `name`='$name' AND `key`='$key'");
			if(mysql_num_rows($result)==0)
				dbQuery($sqlInsert);
			else {
				dbQuery($sqlUpdate);
			}
			self::$loadedData[$name][$key] = $data;
			return $data;
		}
		
		public static function getAsArray($name,$key,$insertIfNotExist = false,$def = null)
		{
			$data=self::get($name,$key,$insertIfNotExist,$def);
			return objectToArray($data);
		}
		
	}
	

?>