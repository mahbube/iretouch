<?php

include "config.php";
templ::$title="تغییر رمز عبور";
_get("act");

if($act=="change" and isPost())
{
    include DOC_ROOT . "admin/view/changePassword/change.php";
}

if(isset($_GET['ok'])) templ::success("کلمه عبور تغییر کرد");
templ::hdr();
include DOC_ROOT . "admin/view/changePassword/form.php";
templ::ftr();

?>