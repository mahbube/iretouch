<?php


class auth
{  
  static private $error;
  
  static public function loginForm()
  {
  	//templ::hdr();
    include (DOC_ROOT . "admin/view/login/loginForm.php");
	//templ::ftr();
	exit;
  }

  static public function checkLogin()
  {
	if(!self::isLogin())
	{
		header("location: " . URL_ROOT . "admin/login.php?redirect=" . stripcslashes(templ::curPageURL()));
        exit;
	}		
  }

  static private function checkUser($userName, $password)
  {
    $query = "SELECT * FROM `admin` WHERE `userName`='$userName' AND `password`=PASSWORD('$password')";
    $result = dbQuery($query);
    if ($result)
    {
      if (mysql_num_rows($result) > 0)
      {
        return true;
      }      
    }
    return false;
  }


  static public function login($userName,$password)
  {
    $redirect=isset($_GET['redirect']) ? $_GET['redirect'] : null;
	if(!$userName or !$password)
	{
		self::$error="نام کاربری یا کلمه عبور وارد نشده است";
		return false;
	}
	
    if (self::checkUser($userName, $password))
    {
      $cooki = base64_encode("$userName|$password");
      if (setcookie("admin", $cooki))
      {
      	header('Location: ' . (is_null($redirect) ? "index.php" : $redirect));
        exit;
	  }	
      else
      {        
        self::$error .= "وارد نمی شوید";
        return false;
      }
    }
    else
    {        
      self::$error .= "نام کاربری یا رمز عبور اشتباه است";
      return false;
    }
  }
  static public function isLogin()
  {
    $cooki = (isset($_COOKIE['admin'])) ? $_COOKIE['admin'] : null;
    if (!$cooki)
      return false;
    $cooki = base64_decode($cooki);
    $cooki = explode("|", $cooki);
    if (count($cooki) <> 2)
    {
      return false;
    }
    
	$username=$cooki[0];
	$password=$cooki[1];
	
	if(self::checkUser($username, $password))
	{
		return true;
	}
	return false;

  }
  static public function logOut()
  {
     setcookie("admin","",-60);
	 header("location:".URL_ROOT."index.php");
     exit;
  }


}
?>