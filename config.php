<?php
$path=realpath(dirname(__FILE__));
define('DOC_ROOT',$path."/" );
$uii = substr(substr(DOC_ROOT, strlen(realpath($_SERVER['DOCUMENT_ROOT']))),1);

//define('URL_ROOT', "/" . ( empty($uii) ? "" : $uii ) );
define('URL_ROOT', 'http://127.0.0.1/iretouch/');

//define db info
define("HOST","localhost");
define("USERNAME","root");
define("PASSWORD","");
define("DBNAME","gw_iretouch");



// require files in funcs dir
function req($fileNames)
{
  foreach ($fileNames as $fileName)
  {
    $file = DOC_ROOT . "funcs/" . $fileName . ".php";
	
    if (file_exists($file))
      require_once ($file);
  }
}
$files = array("dbFuncs","phpFuncs","jdf","simple_html_dom","fileUpload");
req($files);



// load classes when called
function __autoload($class_name)
{
    $file = DOC_ROOT . 'classes/' . $class_name . '.php';
 	//echo $file;
    if (file_exists($file)) {
        require $file;
        return;
    }
	 throw new Exception('The class ' . $class_name . ' could not be loaded');
}



//====================================================

debugMessage::resetTimer();

//connect to dataBase
if(dbCon(HOST,USERNAME,PASSWORD,DBNAME))
{
	new debugSuccess("connect to dataBase");
}
else {
	 new debugError("can not connect to dataBase");
}


?>