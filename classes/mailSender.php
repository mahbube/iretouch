<?php

class mailSender
{

    private $smtpServer     = "smtp.mail.yahoo.com";
    private $smtpUserName   = "hosseinyaghmaee@yahoo.com";   
    private $smtpPassword   = "";
    private $smtpSecure     = "ssl";
    private $smtpPort       = "445";
    
    private $mailFrom       = "hosseinyaghmaee@yahoo.com";
    private $mailFromName   = "ireTouchPro";
    
    private $mail;
    public function __construct() {
        
        ini_set('display_errors','On');
        //$this->mail = @new KM_Mailer($this->smtpServer, $this->smtpPort, $this->smtpUserName, $this->smtpPassword, $this->smtpSecure);
    }
    
    private function sendSMTP($to,$subject,$text,$file)
    {
        $header=include DOC_ROOT . "view/emailLayout/header.php" ;
        $footer=include DOC_ROOT . "view/emailLayout/footer.php" ;
        $text=$header.$text.$footer;
        if(!file_exists($file)) return false;        
        set_time_limit(120);
        if(!$this->mail->isLogin) return false;
        //$this->mail->addAttachment($file);
        return $this->mail->send($this->mailFrom,$to,$subject,"salam");        
    }
    private function send($to,$subject,$text,$filePath,$file)
    {
        $hdrLayout=include DOC_ROOT . "view/emailLayout/header.php"; 
        $ftrLayout=include DOC_ROOT . "view/emailLayout/footer.php";
        $text=$hdrLayout.$text.$ftrLayout;
        return $this->sendMail($file,$filePath,$to,$this->mailFrom,$this->mailFromName,$this->mailFrom,$subject,$text);
    } 
    
    
    public function adminSendMail($to,$subject,$text,$filePath,$fileName)
    {
        return $this->send($to,$subject,$text,$filePath,$fileName);             
    }
    
    public function contactUsEmail($text,$subject)
    {
        return $this->send($this->mailFrom,$subject,$text,"","");
    }
    
    public function infoToCustomer($to,$text,$subject)
    {
        return $this->send($to,$subject,$text,"","");
    }   
    
    function sendMail($filename, $path, $mailto, $from_mail, $from_name, $replyto, $subject, $message)
     {
        
        $uid = md5(uniqid(time()));
        $file = $path.$filename;
        
        $header = "From: ".$from_name." <".$from_mail.">\r\n";
        $header .= "Reply-To: ".$replyto."\r\n";
        $header .= "MIME-Version: 1.0\r\n";
        $header .= "Content-Type: multipart/mixed; boundary=\"".$uid."\"\r\n\r\n";
        $header .= "This is a multi-part message in MIME format.\r\n";
        $header .= "--".$uid."\r\n";
        $header .= "Content-type:text/html; charset=UTF8\r\n";
        $header .= "Content-Transfer-Encoding: 7bit\r\n\r\n";
        $header .= $message."\r\n\r\n";
        $header .= "--".$uid."\r\n";
        
        if(file_exists($file))
        {        
            $file_size = filesize($file);
            $handle = fopen($file, "r");
            $content = fread($handle, $file_size);
            fclose($handle);
            $content = chunk_split(base64_encode($content));
            $name = basename($file);        
            
            $header .= "Content-Type: application/octet-stream; name=\"".$filename."\"\r\n"; // use different content types here
            $header .= "Content-Transfer-Encoding: base64\r\n";
            $header .= "Content-Disposition: attachment; filename=\"".$filename."\"\r\n\r\n";
            $header .= $content."\r\n\r\n";
        }
        $header .= "--".$uid."--";
        return @mail($mailto, $subject, "", $header);
    }
    
    
    
    
}


?>