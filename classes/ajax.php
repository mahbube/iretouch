<?php
    

class ajax 
{
    private static $isajax     = false;
    private static $check_auth = false;
    private static $auth       = false;
    private static $data       = array("errors"=>array(),"debug"=>array());
    
    public function __construct()
    {
        
    }
    public static function isAjax($exit=true)
    {
        if(!(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')))
        {
            if($exit)
            {
                templ::errorPage("ajax");
                exit;
            }
            else return false;
        }
        return true;
    }
    public static function addError($error)
    {
        self::$data["errors"][] = $error;
    }
    public static function addErrors($errors)
    {
        self::$data["errors"] = array_merge(self::$data["errors"],$errors);
    }
    public static function addData($data,$key = null,$parent = false)
    {
        if( !$parent )
            self::$data[$key] = $data;
        else
            self::$data[$key][] = $data;
    }
    
    public static function output()
    {
        self::$data["debug"] = debug::returnArray();
        header('Content-type: text/json');
        header('Content-type: application/json');
        
        echo json_encode(self::$data);

    }
    
}