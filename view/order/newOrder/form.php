<?php
$checked=array("1"=>false,"2"=>false,"3"=>false,"4"=>false);
$serviceType=(isPost()) ? $_POST['serviceType'] : 2;
_var("cusFirstName,cusLastName,cusEmail,cusTel,serviceType,onlyMiss,comment,serviceType");
$checked[$serviceType]=true;
$missChecked=($onlyMiss=="on") ? true : false; 

?>

<script type="text/javascript">


    $(document).ready(function(){
       
        $("#step1").fadeIn(300);
        $("#submitForm").click(function(){
                
                if(validate.check())
                {
                    $("#newOrderForm").submit();
                    $("#step2").fadeOut(50,function(){$("#step3").fadeIn(300);});  
                }
                //       
            
        });
        $("a.next").click(function(){
            var target="#"+$(this).data("target");
            var curunt="#"+$(this).data("curunt");
            $(curunt).fadeOut(50,function(){
                $(target).fadeIn(300);    
            });           
        });        
    });

</script>

<div class="newOrder">
    <form id="newOrderForm" action="?act=confirm" method="post" enctype="multipart/form-data">
    <!-- start step1 --> 
        <div class="step" id="step1">        
                <div class="inside-content order">
                  <span class="order-title font-face">سرویس ها:</span>
                  <span class="font-face">1- روتوش ساده: (5000 تومان)</span>
                  <p>شـــــــامل ( حـــذف لکـــــه های پــــــوست ، ســـــفیـد کــــردن دنــدان هــــا و چشم هــــا  ،
                    حــــذف تـــابش هـــــــای خیـــــره کننــــــده صــــــورت ، حــــذف موهــــــای اضـــــافی  و ...)</p>
                    <span class="font-face">2 - روتوش متوسط : ( 10000 تومان )</span>
                  <p>شـــامل ( تمــام موارد روتـوش ساده ، شکل دادن صــورت و بـدن ، حـذف چیــن و چروک ،
                    تغییر رنگ چشم ، باز کردن چشم ها ، حذف تابش در عینک و ... )</p>
                    <span class="font-face">3 - روتوش حرفه ای : ( 15000 تومان )</span>
                  <p>شامل ( تمام موارد فوق ، حذف پس زمینه ، تغییر رنگ لباس ، حذف اشیاء اضافی در عکس )</p>
                  <span class="font-face">4 - روتوش فانتزی : ( 20000 تومان )</span>
                  <p>شامل ( تمام روتوشهای ذکر شده ، میکاپ کامل صورت ، تیره گی و روشنی پوست به صورت
                    دستی و کاملا حرفه ای ، لیفتینگ صورت به صورت Fashion و ...)</p><br>  
                          <div class="services">
                                         <label id="service"><span class="font-face">انتخاب سرویس :</span></label>
                                         <label class="radio_btn"><span class="font-face">1)</span><input type="radio" name="serviceType" <?php echo ($checked["1"]) ? "checked=\"checked\" " : "" ?> value="1" /></label>
                                         <label class="radio_btn"><span class="font-face">2)</span><input type="radio" name="serviceType" <?php echo ($checked["2"]) ? "checked=\"checked\" " : "" ?> value="2" /></label>
                                         <label class="radio_btn"><span class="font-face">3)</span><input type="radio" name="serviceType" <?php echo ($checked["3"]) ? "checked=\"checked\" " : "" ?> value="3" /></label>
                                         <label class="radio_btn"><span class="font-face">4)</span><input type="radio" name="serviceType" <?php echo ($checked["4"]) ? "checked=\"checked\" " : "" ?> value="4" /></label>
                          </div>
                     <label id="msg"><textarea name="comment" cols="" rows="" placeholder="توضیحات تکمیلی مربوط به سفارش"><?php echo $comment; ?></textarea></label>
        			<a data-target="step2" data-curunt="step1" class="next font-face next1">بعد</a>
                </div>
            </div>
        
        <!-- /step1 -->
        
        <!-- step2 -->
        <div class="step" id="step2">
        
                <div class="inside-content order2">
                    <label>
                        <span class="font-face">نام :</span>
                        <input name="cusFirstName" id="cusFirstName" value="<?php echo $cusFirstName ?>" type="text">
                    </label>
                    <label>
                        <span class="font-face">نام خانوادگی :</span>
                        <input name="cusLastName" id="cusLastName" value="<?php echo $cusLastName ?>" type="text">
                    </label>
                    <label>
                        <span class="font-face">ایمیل :</span>
                        <input id="cusEmail" type="text" value="<?php echo $cusEmail ?>" name="cusEmail">
                    </label>
                     <label>
                        <span class="font-face">تلفن :</span>
                        <input name="cusTel" alt="num" id="cusTel" value="<?php echo $cusTel ?>" type="text">
                    </label>
                     <label>
                        <span class="font-face">فایل :</span>
                        <input name="file" id="file" type="file">
                    </label>             
                     <label id="women">
                        <span class="font-face">ویرایش فقط توسط بانو انجام شود</span>
                        <input name="onlyMiss" <?php echo ($missChecked) ? "checked=\"checked\"" : "" ?> type="checkbox"></label>
                     <a id="submitForm" class="next font-face">ارسال</a>
                     <a data-target="step1" data-curunt="step2" class="next font-face">قبل</a>        		  
                    <div class="clear"></div>
                </div>        
        </div>
        <!-- /step2 -->


        <!-- step3 -->
        <div class="step" id="step3">
        
                <div class="inside-content order2" style="padding-top: 250px">
                   <div class="font-face" style="text-align: center; font-size: 17px; ">لطفا صبر کنید ... </div>  		  
                   <div class="clear"></div>
                </div>        
        </div>
        <!-- /step3 -->
               
    </form>
</div>
