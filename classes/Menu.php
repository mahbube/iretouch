<?php
	// 
	//  menu.php
	//  Emadgh.net
	//  
	//  Created by Emad Ghasemmi on 2012-01-25.
	//  Copyright 2012 Emad Ghasemi. All rights reserved.
	// 
	
	class Menu
	{
		private $id 			= "";
		private $class			= "";
		private $items 			= array();
		public  $path 			= array();
		public 	$path_w 		= 0;
		public 	$number_menus	= 0;
		public  $options		= array(
				"b_menu"				=> "<ul id='%s' class='%s'>",
				"a_menu"				=> "</ul>",
				"b_menuItem"			=> "<li %s>",
				"a_menuItem"			=> "</li>",
				"b_submenu"				=> "<ul class='submenu'>",
				"a_submenu"				=> "</ul>",
				"b_submenuItem"			=> "<li></li class='begin'><li>",
				"a_submenuItem"			=> "</li><li class='end'></li>",
				
				"submenu_only_current"	=> false,
			);
		/**
		 * @id : Id of <ul> object.
		 * @class : class of <ul> object.
		 */
		function __construct($id = "",$class = "")
		{
			$this->id		= $id;
			$this->class 	= $class;
		}

		function add(menuItem $menuItem)
		{
			$this->items[] = $menuItem;
		}

		function render()
		{
			echo vsprintf($this->options['b_menu'],array($this->id, $this->class));
			
			foreach ($this->items as $menuItem)
			{
				$menuItem->render($this);
			}
			
			echo $this->options['a_menu'];
		}
		function setCurrent($path)
		{
			$this->path	= explode("/", $path);
		}
		function setOptions($data)
		{
			$this->options	= array_merge($this->options,(array)$data);
		}
		
	}

	/**
	 * Menu Item
	 */
	class menuItem
	{
		public $text 		= "";
		public $href 		= "";
		public $title 		= "";
		public $class 		= "";
		public $submenus	= array();
		private $root		= null;
		/**
		 * @data : array(
		 * 		"text" => "",
		 * 		"href" => "",
		 * 		"title" => "",
		 * 		"class" => "",
		 * 		"submenu" => new menuItem(...)
		 * )
		 */
		//function __construct(array $data)
		function __construct($text = "",$href = "#",$title = "",$class = "",$submenu = null)
		{
			$this->text		= $text;
			$this->href		= $href;
			$this->title	= $title;
			$this->class 	= $class;
			$this->submenus	= $submenu;
		}
		function render($root = null)
		{
			$this->root	= $root;
			$class 	= "";
			if(count($this->root->path) > $this->root->path_w)
			{
				if($this->root->path[$this->root->path_w] == $this->text)
				{
					$class =  "current";
					$this->root->path_w ++;
				}
			}
			/*
			foreach ($root->path as $way) {
				$class = $way == $this->text ? "current" : "";
				continue;
			}
			*/
            echo sprintf($this->root->options['b_menuItem'],!empty($this->submenus) ? "class='has_sub'" : "");
			echo "<a href='$this->href' class='$this->class $class'>$this->text</a>";
			
			if($this->root->options["submenu_only_current"])
			{
				if($class == "current")
					$this->submenuRender($class);
			}
			else {
				$this->submenuRender($class);
			}
			echo $this->root->options['a_menuItem'];
		}
		private function submenuRender($class)
		{
			
			if(!empty($this->submenus))
			{
				echo $this->root->options['b_submenu'];
				foreach ($this->submenus as $submenu) 
				{
					$submenu->render($this->root);
				}
				echo $this->root->options['a_submenu'];
			}
			
		}
		
	}
?>