<?php

//
//  date.php
//  Orders Management System
//
//  Created by HSY on 2012-03-29.
//  Copyright 2012 HSY. All rights reserved.
//

//

class jdc
{

	static function strToTimeStamp($date)
	{
		$date = explode("/", $date);
		if (count($date) != 3)
			return null;
		$m = $date[1];
		$d = $date[0];
		$y = $date[2];

		$time = jMktime(0, 0, 0, $date[1], $date[0], $date[2]);
		new debugError($time);
		return $time;
	}

	static function showDate($timeStamp = "", $format = "")
	{
		if ($timeStamp <> "" and !self::isValidTimeStamp($timeStamp))
			return null;
		$timeStamp = ($timeStamp <> "") ? $timeStamp : time();
		if ($format == "")
			$format = options::get("format", "date", true, "Y-m-d");
		return jdate($format, $timeStamp);
	}

	static function showTime($timeStamp = "", $format = "")
	{
		if ($timeStamp <> "" and !self::isValidTimeStamp($timeStamp))
			return null;
		$timeStamp = ($timeStamp <> "") ? $timeStamp : time();
		if ($format == "")
			$format = options::get("format", "time", true, "H:i:s");
		return jdate($format, $timeStamp);
	}

	static function showDateTime($timeStamp = "", $format = "")
	{
		if ($timeStamp <> "" and !self::isValidTimeStamp($timeStamp))
			return null;
		$timeStamp = ($timeStamp <> "") ? $timeStamp : time();
		if ($format == "")
			$format = options::get("format", "dateTime", true, "Y-m-d H:i:s");
		return jdate($format, $timeStamp);
	}

	public static function isValidTimeStamp($timestamp)
	{
		return ((string)(int)$timestamp === $timestamp) && ($timestamp <= PHP_INT_MAX) && ($timestamp >= ~PHP_INT_MAX);
	}

	static function checkDate($date)
	{
		
		$date = explode("/", $date);
		
		$m = $date[1];
		$d = $date[0];
		$y = $date[2];
		
		if(!jcheckdate($m, $d, $y))
			return false;
		$time = jMktime(0, 0, 0, $m, $d, $y);		
		return self::isValidTimeStamp("$time");
	}

}
?>