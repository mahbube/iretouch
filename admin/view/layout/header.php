<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en" dir="rtl">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<link rel="shortcut icon" href="<?php echo URL_ROOT ?>resources/image/icon.png" />
		<title><?php echo $title ?></title>
		<script>
			var url_root = "<?php echo URL_ROOT ?>";
            var url_cur = "<?php echo $_SERVER["PHP_SELF"] ?>";
		</script>
		<?php self::writeResources() ?>
	</head>
	<body>
    <div class="wrapper">
     <div class="main-menu">
        <div class="menu font-face">
          <ul>
            <li><a href="<?php echo URL_ROOT ."admin/viewOrders.php" ?>">سفارشات</a></li>
            <li><a href="<?php echo URL_ROOT ."admin/payments.php" ?>">پرداخت ها</a></li>
            <li><a href="<?php echo URL_ROOT ."admin/changePassword.php" ?>">تغییر رمز عبور</a></li>
            <li><a href="<?php echo URL_ROOT ."admin/logOut.php" ?>">خروج</a></li>
          </ul>
        </div>
      </div>         
				<?php
					self::printErr(self::$error);
					self::printSuc(self::$success);
				?>   
			<div class="adminContent">