<?php
//
//  validation.php
//  Orders Management System
//
//  Created by HSY on 2012-03-18.
//  Copyright 2012 HSY. All rights reserved.
//

class validation
{
	public $errors = array();
	private $rules 	= array();
	
	function addError($err)
	{
		//new debugError($err);
		$this->errors[]=$err;
	}
	
    /**
     * New Rule
     * 
     * @Label as String
     * @value as String
     * @contitions as Array
     */
	public function addRule($label,$value,$conditions)
	{
		$this->rules[]=array($label,$value,$conditions);
	}
	
	public function run()
	{
		foreach ($this->rules as $item)
		{
			$this->validateItem($item);
		}
		return empty($this->errors);
	}
	
	function validateItem($item)
	{
		$label		=$item[0];
		$value		=$item[1];
		$conditions	=$item[2];	
		
		if(isset($conditions['trim']))
		{
			$value=trim($value);
		}
		
		if(isset($conditions['req']))
		{			
			if($value=="")
			{
				$this->addError($label." وارد نشده است");
				return false;				
			}
		}	
		
		foreach ($conditions as $conditionName => $conditionValue)
		{			
			switch ($conditionName) {
				case "type":					
					if(!$this->checkType($conditionValue,$value))
					{						
						$this->addError($label." صحیح وارد نشده است ");
					}
					break;
					
				case 'maxLen':
					if($this->maxLen($value,$conditionValue))
						$this->addError("طول ".$label." بیشتر از ".$conditionValue . " کاراکتر است ");
					break;
				
				case 'minLen':
					if($this->minLen($value,$conditionValue))
						$this->addError("طول ".$label."  کمتر از ".$conditionValue . " کاراکتر است ");
					break;
                    	case 'minInt':
					if($this->checkType("int",$value))
					{
					   if($value<$conditionValue)
                            $this->addError("مقدار ".$label." از ".$conditionValue . " کمتراست");
					}
                    else $this->addError($label." باید عدد باشد");
                    break;	
                
                case 'maxInt':
					if($this->checkType("int",$value))
					{
					   if($value>$conditionValue)
                            $this->addError("مقدار ".$label." از ".$conditionValue . " بیشتر است ");
					}
                    else $this->addError($label." باید عدد باشد");
                    break;   
				case 'date':									
					if(!$this->validateDate($value))
						$this->addError($label. " اشتباه وارد شده است");					
					break;
				case 'mail':									
					if(!$this->validMail($value))
						$this->addError($label. " اشتباه وارد شده است");					
					break;	
					
					
				case "call":                    
					//$conditionValue($value,$this);                    
					break;
					
			}
			
		}
	}
	
	function checkType($type,$value)
	{
		switch ($type) {
			case 'num':
				return is_numeric($value);
			
			case "int":				
				return is_numeric($value) ? is_int((int)$value) : false;
			
			case "str":
				return (!is_numeric($value) or $value == "" );
		}
	}

	function maxLen($string,$len)
	{
		if(!is_string($string))
			return false;
		if(strlen($string)>$len)
			return false;
		return true;
	}	
	
	function minLen($string,$len)
	{
		if(!is_string($string))
			return false;
		if(strlen($string)>$len)
			return false;
		return true;
	}	
	
	
	
	function validMail($addr)
	{
		if ((!ereg(".+\@.+\..+", $addr)) || (!ereg("^[a-zA-Z0-9_@.-]+$", $addr)))
			RETURN FALSE;
		else
			RETURN TRUE;
	}

	public static function validateDate($date)
	{
		return jdc::checkDate($date);
		
		//$date = explode("/", $date);
		//if (count($date) != 3 ) return false;
		//return jcheckdate($date[1], $date[0], $date[2]);

	}

}
?>
