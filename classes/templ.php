<?php

class templ
{

    private static $error = null;
    private static $success = null;
    /** This is shit */
    public static $menus = array();
    public static $innerMenuCurunt = -1;
    public static $MenuCurunt = -1;
    public static $innerMenu = array();

    public static $title = "ireTouchPro";
    public static $pageTitle = "";

    private static $csses = array("jquery.hoverscroll","inside-style","jquery.simplyscroll");
    private static $jses = array(
        "jquery-1.7.1.min",
        "scripts",
        "jquery.simplyscroll",
        "jquery.hoverscroll",
        "css_browser_selector_v2",
        
        
    );
    private static $countTips = 0;
    public static $footer_c = ""; 
    
    public static function addCss($css)
    {
        self::$csses[] = $css;
    }

    public static function addJs($js)
    {
        self::$jses[] = $js;
    }

    public static function writeResources()
    {
        foreach (self::$csses as $value) {

            if (file_exists(DOC_ROOT . "resources/css/$value.css")) {

                echo "\n    <link href='" . URL_ROOT . "resources/css/$value.css' rel='stylesheet' type='text/css' />";
            }
        }

        foreach (self::$jses as $value) {
            if (file_exists(DOC_ROOT . "resources/scripts/$value.js")) {

                echo "\n    <script language='JavaScript' src='" . URL_ROOT .
                    "resources/scripts/$value.js'></script>";
            }
            else
            {
                echo "\n    <script language='JavaScript' src='$value'></script>";
            }
        }
    }

    public static function pageTitle($title = "")
    {
        if (!empty($title))
            self::$pageTitle = $title;

        if (self::$pageTitle) {
?>
			 <span class="pageTitle"><?php echo self::$pageTitle ?><span class="end"></span></span>
			<?php
        }
    }

    public static function printErr($err)
    {
        
        if ($err) {
            
            $errs = explode("<br />", $err);
?>
  	 <div class="error">
  	 	<ul>
	  	 <?php
            foreach ($errs as $err)
                if ($err)
                    echo "<li>" . $err . "</li>";
?>
     	</ul>
     </div>
     <?php
        }

    }
    
    public static function printSuc($suc)
    {
        if ($suc) {
            $sucs = explode("<br />", $suc);
?>
  	 <div class="success">
  	   <ul><?php
            foreach ($sucs as $suc)
                if ($suc)
                    echo "<li>" . $suc . "</li>";
?></ul>
     </div>
     <?php
        }
    }

    //render navigtor
    static public function navigator()
    {
        self::$menus->render();
    }

    public static function hdr()
    {
        $title = self::$title;
        global $isAdmin;
        $admin=($isAdmin)?"admin/" : "";
        include (DOC_ROOT . "./".$admin."view/layout/header.php");
    }
    public static function ftr()
    {
        global $isAdmin;
        $admin=($isAdmin)?"admin/" : "";
        include (DOC_ROOT . "./".$admin."view/layout/footer.php");
    }

    public static function kill($err)
    {
        templ::error($err);
        templ::hdr();
        templ::ftr();
        exit;
    }


    public static function error($err)
    {
        
        if (is_array($err))
        {
            foreach($err as $errr)
            {
                self::$error .= $errr . "<br />";      
            }
        }
        else
        {
            self::$error .= $err . "<br />";
        }
        
    }
    public static function success($suc)
    {
        if (is_array($suc))
        {
            foreach($suc as $succ)
            {
                self::$success .= $succ. "<br />";      
            }
        }
        else
        self::$success .= $suc . "<br />";
    }
  

    function curPageURL()
    {
        $pageURL = 'http';
        $pageURL .= "://";
        if ($_SERVER["SERVER_PORT"] != "80") {
            $pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . $_SERVER["REQUEST_URI"];
        } else {
            $pageURL .= $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
        }
        return $pageURL;
    }
	

    static public function Tip($content)
    {  
        $countTips = self::$countTips++; 
        echo "<a data-tip='#tip_$countTips' class='tip'></a>";
        self::$footer_c .= "<div class='tip_c hidden' id='tip_$countTips' >$content</div>";
    }
}
?>







