<?php 
    _post("cusFirstName,cusLastName,cusEmail,cusTel,serviceType,onlyMiss,comment,serviceType",true);
    _null("err");
    $validation = new validation();
    $validation->addRule("نام",$cusFirstName,array("req"=>true,"trim"=>true));    
    $validation->addRule("نام خانوادگی",$cusLastName,array("req"=>true,"trim"=>true));   
    $validation->addRule("ایمیل",$cusEmail,array("req"=>true,"trim"=>true,"mail"=>true)); 
    $validation->addRule("تلفن",$cusTel,array("req"=>true,"trim"=>true,"type"=>"int"));
    $ok=$validation->run();
    
    $onlyMissT=($onlyMiss=="on") ? 1 :0;
    
    //$timeStamp=time();
    $factorNumber=$_POST['factorNumber'];
    $fileName=$_POST['fileName'];
    $timeStamp=$_POST['timeStamp'];
    if($ok){
        $y= jdate("Y",null,null,null,"en");$m= jdate("m",null,null,null,"en");$d= jdate("d",null,null,null,"en");
            //prepare uplodig path :  year as $y/ mounth as $m  / day as $d 
        $cf=chkFile("file") ;
        if($cf["success"]){        

            //$now=jdate("His",null,null,null,"en");
            //$fileName="$now-$factorNumber";
            
            
            if(!file_exists(DOC_ROOT . "uploads/$y")) 
            {
                mkdir(DOC_ROOT . "uploads/$y");
                // copy index.php to created directory
                copy(DOC_ROOT . "view/index.php" , DOC_ROOT . "uploads/$y/index.php");
            }
            if(!file_exists(DOC_ROOT . "uploads/$y/$m")) 
            {
                mkdir(DOC_ROOT . "uploads/$y/$m");
                copy(DOC_ROOT . "view/index.php" , DOC_ROOT . "uploads/$y/$m/index.php");
            }
            if(!file_exists(DOC_ROOT . "uploads/$y/$m/$d")) 
            {
                mkdir(DOC_ROOT . "uploads/$y/$m/$d");
                copy(DOC_ROOT . "view/index.php" , DOC_ROOT . "uploads/$y/$m/$d/index.php");
            }
            //upload file
            $f=fileUploader("file",$fileName,"$y/$m/$d");
            if($f["success"])
            {
                $fileName=$f['fileName'];
            }
            else
            {
                $ok=false;
                templ::error($f['message']);
            }
        }else{
            $fileName="$y/$m/$d/".$fileName ;
        }
    }
    if($ok)
    {        
    	$insertSqlArray = array(
            "factorNumber"	=>	"$factorNumber",
    		"cusFirstName"	=>	"'$cusFirstName'",
    		"cusLastName"	=>	"'$cusLastName'",
            "cusEmail"      =>  "'$cusEmail'",
            "cusTel"        =>  "'$cusTel'",
            "onlyMiss"      =>  "$onlyMissT",
            "orderStatus"   =>  "'recieved'",
            "orderTime"   =>    $timeStamp,
            "fileName"      =>  "'$fileName'",
            "serviceType"   =>  "'$serviceType'",
            "comment"	    =>	"'$comment'"
    	);
    
      $insertSql = insertSql($insertSqlArray);
      $query = "insert into `order` " . $insertSql;
      $result = dbQuery($query);
      if (!$result)
      {
        templ::error(mysql_error());
        templ::error($query);
      }
      else
      {        
        $mail=new mailSender();
        $text="سفارش شما دریافت گردید </br> شماره فاکتور : <b>$factorNumber</b><br /> ";
        $text.="برای اطلاع از وضعیت سفارش به <a style='color: blue' href='".URL_ROOT."follow.php?act=follow&factorNumber=$factorNumber&cusEmail=$cusEmail'> این لینک </a>مراجعه نمایید";
        if($mail->infoToCustomer($cusEmail,$text,"اطلاعات سفارش"))
            templ::success("برای شما ایمیل ارسال گردید");
        
        
        
        templ::hdr();
        include DOC_ROOT . "view/order/newOrder/success.php";
        templ::ftr();
        exit ;
      }
    }
    else
    {
      templ::error($validation->errors);
    }
    
?>