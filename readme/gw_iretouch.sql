-- phpMyAdmin SQL Dump
-- version 3.2.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 05, 2013 at 10:49 AM
-- Server version: 5.1.41
-- PHP Version: 5.3.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `gw_iretouch`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userName` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `nickName` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `userName`, `password`, `nickName`) VALUES
(1, 'green', '*AC5CB9F7AFF77B104B044F1B7D0FB115C934D976', 'green'),
(2, 'admin', '*4ACFE3202A5FF5CF467898FC58AAB1D615029441', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `order`
--

CREATE TABLE IF NOT EXISTS `order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `factorNumber` int(11) NOT NULL,
  `cusFirstName` varchar(100) CHARACTER SET utf8 COLLATE utf8_persian_ci NOT NULL,
  `cusLastName` varchar(100) CHARACTER SET utf8 COLLATE utf8_persian_ci NOT NULL,
  `cusTel` varchar(30) CHARACTER SET utf8 COLLATE utf8_persian_ci NOT NULL,
  `cusEmail` varchar(100) CHARACTER SET utf8 COLLATE utf8_persian_ci NOT NULL,
  `onlyMiss` tinyint(1) NOT NULL,
  `serviceType` varchar(50) CHARACTER SET utf8 COLLATE utf8_persian_ci NOT NULL,
  `comment` text CHARACTER SET utf8 COLLATE utf8_persian_ci NOT NULL,
  `adminMessage` varchar(255) CHARACTER SET utf8 COLLATE utf8_persian_ci NOT NULL,
  `fishNumber` varchar(30) CHARACTER SET utf8 COLLATE utf8_persian_ci NOT NULL,
  `fishStatus` varchar(30) CHARACTER SET utf8 COLLATE utf8_persian_ci NOT NULL DEFAULT 'waiting',
  `orderStatus` varchar(30) CHARACTER SET utf8 COLLATE utf8_persian_ci NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `orderTime` int(11) NOT NULL,
  `new` tinyint(1) NOT NULL DEFAULT '1',
  `fileName` varchar(100) CHARACTER SET utf8 COLLATE utf8_persian_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1080 ;

--
-- Dumping data for table `order`
--

INSERT INTO `order` (`id`, `factorNumber`, `cusFirstName`, `cusLastName`, `cusTel`, `cusEmail`, `onlyMiss`, `serviceType`, `comment`, `adminMessage`, `fishNumber`, `fishStatus`, `orderStatus`, `deleted`, `orderTime`, `new`, `fileName`) VALUES
(1000, 1360395088, 'Ø­Ø³ÛŒÙ†', 'ÛŒØºÙ…Ø§ÛŒÛŒ', '246811', 'hosseinyaghmaee@yahoo.com', 1, '4', '', '', '', 'waiting', 'recieved', 1, 1360395088, 0, '1391/11/21/110128-1360395088.jpg'),
(1001, 1360395262, 'Ø­Ø³ÛŒÙ†', 'ÛŒØºÙ…Ø§ÛŒÛŒ', '246811', 'hosseinyaghmaee@yahoo.com', 1, '4', '', '', '', 'waiting', 'recieved', 1, 1360395262, 1, '1391/11/21/110422-1360395262.jpg'),
(1002, 1360395270, 'Ø­Ø³ÛŒÙ†', 'ÛŒØºÙ…Ø§ÛŒÛŒ', '246811', 'hosseinyaghmaee@yahoo.com', 1, '4', '', '', '', 'waiting', 'recieved', 1, 1360395270, 1, '1391/11/21/110430-1360395270.jpg'),
(1003, 1360395275, 'Ø­Ø³ÛŒÙ†', 'ÛŒØºÙ…Ø§ÛŒÛŒ', '246811', 'hosseinyaghmaee@yahoo.com', 1, '4', '', '', '', 'waiting', 'recieved', 1, 1360395275, 1, '1391/11/21/110435-1360395275.jpg'),
(1004, 1360395279, 'Ø­Ø³ÛŒÙ†', 'ÛŒØºÙ…Ø§ÛŒÛŒ', '246811', 'hosseinyaghmaee@yahoo.com', 1, '4', '', '', '', 'waiting', 'recieved', 1, 1360395279, 1, '1391/11/21/110439-1360395279.jpg'),
(1005, 1360395282, 'Ø­Ø³ÛŒÙ†', 'ÛŒØºÙ…Ø§ÛŒÛŒ', '246811', 'hosseinyaghmaee@yahoo.com', 1, '4', '', '', '', 'waiting', 'recieved', 1, 1360395282, 1, '1391/11/21/110442-1360395282.jpg'),
(1006, 1360395286, 'Ø­Ø³ÛŒÙ†', 'ÛŒØºÙ…Ø§ÛŒÛŒ', '246811', 'hosseinyaghmaee@yahoo.com', 1, '4', '', '', '', 'waiting', 'mailed', 1, 1360395286, 1, '1391/11/21/110446-1360395286.jpg'),
(1007, 1360408799, 'Ø­Ø³ÛŒÙ†', 'ÛŒØºÙ…Ø§ÛŒÛŒ', '213132', 'hosseinyaghmaee@yahoo.com', 1, '3', 'Ø´Ø³ÛŒØ´ÛŒØ³Ø´Ø³ÛŒØ´Ø³ÛŒ', '', '', 'waiting', 'recieved', 1, 1360408799, 1, '1391/11/21/144959-1360408799.jpg'),
(1008, 1360409611, 'Ø­Ø³ÛŒÙ†', 'ÛŒØºÙ…Ø§ÛŒÛŒ', '213132', 'hosseinyaghmaee@yahoo.com', 1, '3', 'Ø´Ø³ÛŒØ´ÛŒØ³Ø´Ø³ÛŒØ´Ø³ÛŒ', '', '', 'waiting', 'recieved', 1, 1360409611, 1, '1391/11/21/150331-1360409611.jpg'),
(1009, 1360409766, 'Ø­Ø³ÛŒÙ†', 'ÛŒØºÙ…Ø§ÛŒÛŒ', '213132', 'hosseinyaghmaee@yahoo.com', 1, '3', 'Ø´Ø³ÛŒØ´ÛŒØ³Ø´Ø³ÛŒØ´Ø³ÛŒ', '', '', 'waiting', 'recieved', 1, 1360409766, 1, '1391/11/21/150606-1360409766.jpg'),
(1010, 1360409825, 'Ø­Ø³ÛŒÙ†', 'ÛŒØºÙ…Ø§ÛŒÛŒ', '213132', 'hosseinyaghmaee@yahoo.com', 1, '3', 'Ø´Ø³ÛŒØ´ÛŒØ³Ø´Ø³ÛŒØ´Ø³ÛŒ', '', '', 'waiting', 'recieved', 1, 1360409825, 1, '1391/11/21/150705-1360409825.jpg'),
(1011, 1360409875, 'Ø­Ø³ÛŒÙ†', 'ÛŒØºÙ…Ø§ÛŒÛŒ', '213132', 'hosseinyaghmaee@yahoo.com', 1, '3', 'Ø´Ø³ÛŒØ´ÛŒØ³Ø´Ø³ÛŒØ´Ø³ÛŒ', '', '', 'waiting', 'recieved', 1, 1360409875, 0, '1391/11/21/150755-1360409875.jpg'),
(1012, 1360409948, 'Ø­Ø³ÛŒÙ†', 'ÛŒØºÙ…Ø§ÛŒÛŒ', '213132', 'hosseinyaghmaee@yahoo.com', 1, '3', 'Ø´Ø³ÛŒØ´ÛŒØ³Ø´Ø³ÛŒØ´Ø³ÛŒ', '', '', 'waiting', 'recieved', 1, 1360409948, 1, '1391/11/21/150908-1360409948.jpg'),
(1013, 1360410277, 'Ø­Ø³ÛŒÙ†', 'ÛŒØºÙ…Ø§ÛŒÛŒ', '213132', 'hosseinyaghmaee@yahoo.com', 1, '3', 'Ø´Ø³ÛŒØ´ÛŒØ³Ø´Ø³ÛŒØ´Ø³ÛŒ', '', '', 'waiting', 'recieved', 1, 1360410277, 0, '1391/11/21/151437-1360410277.jpg'),
(1014, 1360474313, 'Ù…ÛŒØªØ²Ø§', 'Ø³ØªÛŒÙ„Ø§', 'ØªØ§', 'rastegar@greenweb.ir', 1, '2', '4', '', '1360474313', 'waiting', 'recieved', 1, 1360474313, 0, '1391/11/22/090153-1360474313.jpg'),
(1015, 1360558376, 'Ù…ÛŒØªØ±Ø§', 'Ø±Ø³ØªÚ¯Ø§Ø± Ù…Ù‚Ø¯Ù… Ø§Ø¹ØªÙ…Ø§Ø¯ÛŒ', '8819714', 'mitra.rastegar.moghadam@gmail.com', 1, '2', '', '', '', 'waiting', 'recieved', 1, 1360558376, 0, '1391/11/23/082256-1360558376.jpg'),
(1016, 1360568489, 'mustafa', 'abedi', '09151040268', 'mustafaabedinasab@gmail.com', 1, '1', '', '', '', 'verified', 'working', 1, 1360568489, 0, '1391/11/23/111129-1360568489.jpg'),
(1017, 1360847947, 'iujuyyt', 'ikutu', '7656987987', 'lkjkg@jygfy.com', 0, '2', '', '', '', 'waiting', 'recieved', 1, 1360847947, 1, '1391/11/26/164907-1360847947.jpg'),
(1018, 1360848181, 'gjhgjh', 'klmkmnk', '5454546', 'akbari.v@gmail.com', 1, '4', '', '', '', 'waiting', 'recieved', 1, 1360848181, 0, '1391/11/26/165301-1360848181.jpg'),
(1019, 1361000590, 'Ø¨Ù„Ø§Ø§ÙØºØ§', 'ÙØºØ§ÙØºØ§Ù‚ÙØ§', '3534234234', 'ergergerg@dfgtg.com', 0, '2', '', '', '', 'waiting', 'recieved', 1, 1361000590, 0, '1391/11/28/111310-1361000590.jpg'),
(1020, 1361066421, 'iu', 'opi', '65678', 'fdgd@b.com', 0, '2', '', '', '', 'waiting', 'recieved', 1, 1361066421, 1, '1391/11/29/053021-1361066421.jpg'),
(1021, 1361208094, 'Hhh', 'Uuyy', '346', 'Yyy@bv.com', 0, '4', '', '', '', 'waiting', 'recieved', 1, 1361208094, 1, '1391/11/30/205134-1361208094.jpg'),
(1022, 1361208758, 'Hf', 'Jh', '444', 'Hamidakbari@gmail.com', 0, '2', '', '', '', 'waiting', 'recieved', 1, 1361208758, 0, '1391/11/30/210238-1361208758.jpg'),
(1023, 1365279314, 'mustafa', 'abedi', '09151040268', 'mustafaabedinasab@gmail.com', 0, '2', '', '', '', 'waiting', 'recieved', 1, 1365279314, 1, '1392/01/18/004514-1365279314.jpg'),
(1024, 1366537014, 'Ù…Ø´ØªØ±ÛŒ', 'Ù…Ø´ØªØ±ÛŒ', '88888888', 'abc@abc.com', 0, '3', 'ØªÙˆØ¶ÛŒØ­', '', '', 'waiting', 'recieved', 0, 1366537014, 0, '1392/02/01/140654-1366537014.jpg'),
(1025, 1368937915, 'Ø¨Ù‡Ù†Ø§Ù…', 'Ú©ØªØ§Ø¨Ø¯Ø§Ø±', '09345443', 'ketabdar@fhg.com', 0, '3', 'ØªØ³Øª Ø³ÙØ§Ø±Ø´ Ø´Ù…Ø§Ø±Ù‡ Û³', '', '', 'waiting', 'recieved', 0, 1368937915, 0, '1392/02/29/090155-1368937915.jpg'),
(1026, 1369515798, 'asdasd', 'asd', '2112313', 'abc@abc.com', 1, '2', '', '', '', 'waiting', 'recieved', 0, 1369515798, 1, '1392/03/05/013318-1369515798.jpg'),
(1027, 1369516113, 'asdasd', 'asd', '2112313', 'abc@abc.com', 1, '2', '', '', '', 'waiting', 'recieved', 0, 1369516113, 1, '1392/03/05/013833-1369516113.jpg'),
(1028, 1369517055, 'asdasd', 'asd', '2112313', 'abc@abc.com', 1, '2', '', '', '', 'waiting', 'recieved', 0, 1369517055, 1, '1392/03/05/015415-1369517055.jpg'),
(1029, 1369517274, 'adsd', 'asd', '2222', 'abc@abc.com', 0, '2', '', '', '', 'waiting', 'recieved', 0, 1369517274, 1, '1392/03/05/015754-1369517274.jpg'),
(1030, 1369533476, 'asd', 'asd', '2313', 'asd@asd.asd', 0, '2', '', '', '', 'waiting', 'recieved', 0, 1369533476, 1, '1392/03/05/062756-1369533476.jpg'),
(1031, 1369533924, 'asd', 'asd', '123', 'asd@asd.asd', 0, '2', '', '', '', 'waiting', 'recieved', 0, 1369533924, 1, '1392/03/05/063524-1369533924.jpg'),
(1032, 1369534872, 'asd', 'asd', '123', 'asd@asd.asd', 1, '2', '', '', '', 'waiting', 'recieved', 0, 1369534872, 1, '1392/03/05/065112-1369534872.jpg'),
(1033, 1369553889, 'Ù…ÛŒØªØ±Ø§', 'Ø±Ø³ØªÚ¯Ø§Ø±', '7678655', 'mitra.rastegar.moghadam@gmail.com', 1, '2', 'raeghgvh', '', 'sad', 'waiting', 'recieved', 0, 1369553889, 1, '1392/03/05/120809-1369553889.jpg'),
(1034, 1369557197, 'sa', 'sattttttttt', '879866', 'mitra.rastegar.moghadam@gmail.com', 1, '3', '', '', '', 'waiting', 'recieved', 0, 1369557197, 0, '1392/03/05/130317-1369557197.jpg'),
(1035, 1369667186, 'Ø¨Ù‡Ù†Ø§Ù…', 'Ú©ØªØ§Ø¨Ø¯Ø§Ø±', '2233445', 'b.ketabdar@gmail.com', 0, '1', 'ØªØ³Øª Ù¾Ø±Ø¯Ø§Ø®Øª Ø¢Ù†Ù„Ø§ÛŒÙ†', '', '', 'waiting', 'recieved', 0, 1369667186, 0, '1392/03/06/193626-1369667186.jpg'),
(1036, 1369792454, 'adsd', 'asd', '222', 'abc@abc.com', 0, '2', '', '', '', 'waiting', 'recieved', 0, 1369792454, 0, '1392/03/08/062414-1369792454.jpg'),
(1037, 1369833796, 'adsd', 'asd', '222', 'abc@abc.com', 0, '2', '', '', '', 'waiting', 'recieved', 0, 1369833796, 1, '1392/03/08/175316-1369833796.jpg'),
(1038, 1370076651, 'Ø¨Ù‡Ù†Ø§Ù…', 'Ú©ØªØ§Ø¨Ø¯Ø§Ø±', '7638100', 'ketabdar@greenweb.ir', 0, '1', 'ØªÙˆØ¶ÛŒØ­Ø§Øª ØªÚ©Ù…ÛŒÙ„ÛŒ Ù…Ø±Ø¨ÙˆØ· Ø¨Ù‡ Ø³ÙØ§Ø±Ø´ Ø¯Ø± Ø§ÛŒÙ† Ù‚Ø³Ù…Øª Ù†ÙˆØ´ØªÙ‡ Ù…ÛŒØ´ÙˆØ¯.', '', '', 'waiting', 'recieved', 0, 1370076651, 0, '1392/03/11/132051-1370076651.jpg'),
(1039, 1370504382, 'Ø¨Ù‡Ù†Ø§Ù…', 'Ú©ØªØ§Ø¨Ø¯Ø§Ø±', '09155199811', 'b.ketabdar@gmail.com', 0, '2', 'ØªØ³Øª', '', '', 'waiting', 'recieved', 0, 1370504382, 1, '1392/03/16/120942-1370504382.jpg'),
(1040, 1370753522, 'ddasd', 'asdasd', '5487989', 'asda@dfsdfs.com', 1, '2', '', '', '', 'waiting', 'recieved', 0, 1370753522, 1, '1392/03/19/092202-1370753522.jpg'),
(1041, 1370755261, 'ddasd', 'asdasd', '5487989', 'asda@dfsdfs.com', 1, '2', '', '', '', 'waiting', 'recieved', 0, 1370755261, 1, '1392/03/19/095101-1370755261.jpg'),
(1042, 1370756878, 'ddasd', 'asdasd', '5487989', 'asda@dfsdfs.com', 1, '2', '', '', '', 'waiting', 'recieved', 0, 1370756878, 1, '1392/03/19/101758-1370756878.jpg'),
(1043, 1370756906, 'ddasd', 'asdasd', '5487989', 'asda@dfsdfs.com', 1, '2', '', '', '', 'waiting', 'recieved', 0, 1370756906, 1, '1392/03/19/101826-1370756906.jpg'),
(1044, 1370756928, 'ddasd', 'asdasd', '5487989', 'asda@dfsdfs.com', 1, '2', '', '', '', 'waiting', 'recieved', 0, 1370756928, 1, '1392/03/19/101848-1370756928.jpg'),
(1045, 1370756930, 'ddasd', 'asdasd', '5487989', 'asda@dfsdfs.com', 1, '2', '', '', '', 'waiting', 'recieved', 0, 1370756930, 1, '1392/03/19/101850-1370756930.jpg'),
(1046, 1370756974, 'ddasd', 'asdasd', '5487989', 'asda@dfsdfs.com', 0, '2', '', '', '', 'waiting', 'recieved', 0, 1370756974, 1, '1392/03/19/101934-1370756974.jpg'),
(1047, 1370756996, 'ddasd', 'asdasd', '5487989', 'asda@dfsdfs.com', 0, '2', '', '', '', 'waiting', 'recieved', 0, 1370756996, 1, '1392/03/19/101956-1370756996.jpg'),
(1048, 1370757055, 'ddasd', 'asdasd', '5487989', 'asda@dfsdfs.com', 0, '2', '', '', '', 'waiting', 'recieved', 0, 1370757055, 1, '1392/03/19/102055-1370757055.jpg'),
(1049, 1370757082, 'ddasd', 'asdasd', '5487989', 'asda@dfsdfs.com', 0, '2', '', '', '', 'waiting', 'recieved', 0, 1370757082, 1, '1392/03/19/102122-1370757082.jpg'),
(1050, 1370757266, 'ddasd', 'asdasd', '5487989', 'asda@dfsdfs.com', 0, '2', '', '', '', 'waiting', 'recieved', 0, 1370757266, 1, '1392/03/19/102426-1370757266.jpg'),
(1051, 1370757338, 'ddasd', 'asdasd', '5487989', 'asda@dfsdfs.com', 0, '2', '', '', '', 'waiting', 'recieved', 0, 1370757338, 1, '1392/03/19/102538-1370757338.jpg'),
(1052, 1370757429, 'ddasd', 'asdasd', '5487989', 'asda@dfsdfs.com', 0, '2', '', '', '', 'waiting', 'recieved', 0, 1370757429, 1, '1392/03/19/102709-1370757429.jpg'),
(1053, 1371548400, 'dsf', 'dsf', '34', 'sdf@gmaiil.con', 0, '2', '', '', '', 'waiting', 'recieved', 0, 1371548400, 1, '1392/03/28/141000-1371548400.jpg'),
(1054, 1375340949, 'fffffff', 'fffffffffffff', '44444444', 'fff@gd.gf', 0, '1', 'tttttttttttttttttttttttttttttt', '', '', 'waiting', 'recieved', 0, 1375340949, 1, '1392/05/10/113909-1375340949.png'),
(1055, 1375343648, '5555555', '555555555', '555555555', 'rdtr@jg.khk', 0, '2', '', '', '', 'waiting', 'recieved', 0, 1375343648, 1, '1392/05/10/122408-1375343648.png'),
(1056, 1375346472, 'yyyyyyy', 'yyyyy', '8419844', 'yhtty@gdf.kk', 0, '2', '', '', '', 'waiting', 'recieved', 0, 1375346472, 1, '1392/05/10/131112-1375346472.jpg'),
(1057, 1375346504, 'yrty', 'rtyry', '75675765', 'yhtty@gdf.kk', 0, '2', '', '', '', 'waiting', 'recieved', 0, 1375346504, 1, '1392/05/10/131144-1375346504.png'),
(1058, 1375350553, 'yyyyyyy', 'yyy', '8419844', 'yhtty@gdf.kk', 0, '2', '', '', '', 'waiting', 'recieved', 0, 1375350553, 1, '1392/05/10/141913-1375350553.png'),
(1059, 1375352927, 'yt', 'y65y5', '75675765', 'yhtty@gdf.kk', 0, '2', '', '', '', 'waiting', 'recieved', 0, 1375352927, 1, '1392/05/10/145847-1375352927.png'),
(1060, 1375353218, 'gg', 'gg', '75675765', 'gg@ere.gj', 0, '2', '', '', '', 'waiting', 'recieved', 0, 1375353218, 1, '1392/05/10/150338-1375353218.png'),
(1061, 1375353433, 'ggg', 'gggg', '666666', 'ghngh@hg.jl', 0, '2', '', '', '', 'waiting', 'recieved', 0, 1375353433, 1, '1392/05/10/150713-1375353433.zip'),
(1062, 1375354033, 'ggg', 'gg', '75675765', 'gg@ere.gj', 0, '2', '', '', '', 'waiting', 'recieved', 0, 1375354033, 1, '1392/05/10/151713-1375354033.rar'),
(1063, 1375515504, 'egter', 'treter', '75675765', 'rt@ghgf.yju', 0, '2', '', '', '', 'waiting', 'recieved', 0, 1375515504, 1, '1392/05/12/120824-1375515504.png'),
(1064, 1375516955, 'Ù„ÛŒØ¨', 'Ù„ÛŒØ¨Ù„', '44444444', 'fsdf@gf.hjk', 0, '2', '', '', '', 'waiting', 'recieved', 0, 1375516955, 1, '1392/05/12/123235-1375516955.png'),
(1065, 1375517952, 'ghjhgj', 'hgjgh', '666666', 'fff@gd.gf', 0, '2', '', '', '', 'waiting', 'recieved', 0, 1375517952, 1, '1392/05/12/124912-1375517952.png'),
(1066, 1375686175, 'hhry', 'hrh', '344435', 'hosseinyaghmaee@yahoo.com', 0, '2', '', '', '', 'waiting', 'recieved', 0, 1375686175, 1, '1392/05/14/113255-1375686175.png'),
(1067, 1375693474, 'hft', 'hbfg', '555555', 'hfghb@jg.uk', 0, '2', '', '', '', 'waiting', 'recieved', 0, 1375693485, 1, '1392/05/14/133434-1375693474.jpg'),
(1068, 1375694494, 'test4', 'test4', '44444444', 'test4@djf.bbb', 1, '4', 'testttttttttttttttttttttttt', '', '', 'waiting', 'recieved', 0, 1375694494, 1, '135134-1375694494'),
(1069, 1375694740, 'gdfg', 'dfgdf', '666666', 'tttttttt@tdfg.jhg', 0, '2', 'gdfg', '', '', 'waiting', 'recieved', 0, 1375694740, 1, '135540-1375694740'),
(1070, 1375694814, 'gjghj', 'ghjhg', '654656565', 'tttttttt@tdfg.jhg', 0, '2', 'gfjgfjh', '', '', 'waiting', 'recieved', 0, 1375694814, 1, '135654-1375694814'),
(1071, 1375694936, 'cxcxc', 'xczxcxzc', '666666', 'sa@dfd.kjh', 0, '2', '', '', '', 'waiting', 'recieved', 0, 1375694936, 1, '1392/05/14/135856-1375694936.jpg'),
(1072, 1375694972, 'scds', 'ffsdf', '555555', 'sdfd@s.fsd', 0, '2', '', '', '', 'waiting', 'recieved', 0, 1375694972, 1, '135932-1375694972'),
(1073, 1375695181, 'fdsfd', 'fdsfsfsdfsd', '44444444', 'FDF@gf.hjk', 0, '2', 'sdfsdfsdf', '', '', 'waiting', 'recieved', 0, 1375695181, 1, '140301-1375695181'),
(1074, 1375695458, 'gdfgfdgdfgh', 'dgdgdg', '555555', 'gg@ere.gj', 0, '2', '', '', '', 'waiting', 'recieved', 0, 1375695458, 1, '1392/05/14/140738-1375695458'),
(1075, 1375695535, 'hygfh', 'jgf', '75675765', 'jgjgj@dfry.jhk', 0, '4', '', '', '', 'waiting', 'recieved', 0, 1375695535, 1, '1392/05/14/140855-1375695535.jpg'),
(1076, 1375695535, 'hygfh', 'jgf', '75675765', 'jgjgj@dfry.jhk', 0, '4', '', '', '', 'waiting', 'recieved', 0, 1375695535, 1, '1392/05/14/140855-1375695535.jpg'),
(1077, 1375695535, 'hygfh', 'jgf', '75675765', 'jgjgj@dfry.jhk', 0, '4', '', '', '', 'waiting', 'recieved', 0, 1375695535, 1, '1392/05/14/140855-1375695535.jpg'),
(1078, 1375695535, 'hygfh', 'jgf', '75675765', 'jgjgj@dfry.jhk', 0, '4', '', '', '', 'waiting', 'recieved', 0, 1375695535, 1, '1392/05/14/140855-1375695535.jpg'),
(1079, 1375695535, 'hygfh', 'jgf', '75675765', 'jgjgj@dfry.jhk', 0, '4', '', '', '', 'waiting', 'recieved', 0, 1375695535, 1, '1392/05/14/140855-1375695535.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

CREATE TABLE IF NOT EXISTS `payment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ordId` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `time` varchar(15) NOT NULL,
  `status` varchar(5) NOT NULL,
  `factorNumber` int(11) NOT NULL,
  `trackId` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `cusName` varchar(100) NOT NULL,
  `comment` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `payment`
--

INSERT INTO `payment` (`id`, `ordId`, `amount`, `time`, `status`, `factorNumber`, `trackId`, `email`, `cusName`, `comment`) VALUES
(1, 1030, 1000, '0000-00-00 00:0', '0', 1369533476, '', 'asd@asd.asd', '', ''),
(2, 1032, 1000, '0000-00-00 00:0', '0', 1369534872, '128946112', 'asd@asd.asd', '', ''),
(3, 1033, 1000, '0000-00-00 00:0', '1', 1369553889, '', 'mitra.rastegar.moghadam@gmail.com', '', ''),
(4, 1034, 1000, '0000-00-00 00:0', '1', 1369557197, '', 'mitra.rastegar.moghadam@gmail.com', '', ''),
(5, 0, 213, '0000-00-00 00:0', '1', 0, '', 'asd@asd.com', '', ''),
(6, 0, 2, '0000-00-00 00:0', '1', 0, '', 'asd@asd.com', '', ''),
(7, 0, 1000, '0000-00-00 00:0', '', 0, '129172643', 'b.ketabdar@gmail.com', '', ''),
(8, 1035, 1000, '0000-00-00 00:0', '0', 1369667186, '129175100', 'b.ketabdar@gmail.com', '', ''),
(9, 0, 0, '0000-00-00 00:0', '1', 0, '', '', '', ''),
(10, 0, 10000, '0000-00-00 00:0', '1', 0, '', 'hosseinyaghmaee@gmail.com', 'hossein', 'asd'),
(11, 0, 10000, '0000-00-00 00:0', '1', 0, '', 'asd@asd.com', 'asdasd', 'asdasd'),
(12, 1036, 1000, '0000-00-00 00:0', '1', 1369792454, '', 'abc@abc.com', '', ''),
(13, 0, 10100, '0000-00-00 00:0', '1', 0, '', 'asd@asd.com', 'asd', ''),
(14, 0, 2312, '0000-00-00 00:0', '1', 0, '', 'asd@asd.com', 'asd', ''),
(15, 0, 2222, '1369833509', '1', 0, '', 'asd@asd.com', 'a', ''),
(16, 1036, 1000, '1369833546', '1', 1369792454, '', 'abc@abc.com', '', ''),
(17, 1036, 10000, '1369833646', '1', 1369792454, '', 'abc@abc.com', '', ''),
(18, 1037, 100000, '1369833819', '1', 1369833796, '', 'abc@abc.com', '', ''),
(19, 1038, 50000, '1370076689', '50', 1370076651, '', 'ketabdar@greenweb.ir', '', ''),
(20, 0, 1000, '1370076981', '0', 0, '129801792', 'b.ketabdar@gmail.com', 'Ø¨Ù‡Ù†Ø§Ù… Ú©ØªØ§Ø¨Ø¯Ø§Ø±', 'ØªØ³Øª ØµÙØ­Ù‡ Ù¾Ø±Ø¯Ø§Ø®Øª'),
(21, 0, 50000, '1370980618', '50', 0, '', 'mustafaabedinasab@yahoo.com', 'Ù…ØµØ·ÙÛŒ', 'Ø³Ø±ÙˆÛŒØ³ Ø´Ù…Ø§Ø±Ù‡ 1');

-- --------------------------------------------------------

--
-- Table structure for table `payment_method`
--

CREATE TABLE IF NOT EXISTS `payment_method` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pay_code` varchar(30) NOT NULL,
  `pin` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `payment_method`
--

INSERT INTO `payment_method` (`id`, `pay_code`, `pin`) VALUES
(1, 'parsian', 'Mm6g5WQp0aYWXu0f7PDE');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
