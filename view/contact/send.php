<?php
_post("email,message,captcha");
$validation=new validation();
$validation->addRule("ایمیل",$email,array("mail"=>true));
$validation->addRule("موضوع",$message,array("req"=>true));
$ok=$validation->run();
if($captcha<>$_SESSION['security_code'])
{
    $ok=false;
    templ::error("کد امنیتی اشتباه وارد شده است"); 
}

if($ok)
{
    $message="ارسال کننده : $email <br>".$message;
    $m=new mailSender();
    if($m->contactUsEmail($message,"ارتباط با ما"))
        templ::success("ارسال شد");
    else
        templ::error("ارسال نشد");    
}
else
{
    templ::error($validation->errors);
}


?>