<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en" dir="rtl">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<link rel="shortcut icon" href="<?php echo URL_ROOT ?>resources/image/icon.png">
        <title><?php echo $title ?></title>
		<script>
			var url_root = "<?php echo URL_ROOT ?>";
            var url_cur = "<?php echo $_SERVER["PHP_SELF"] ?>";
		</script>
		<?php self::writeResources() ?>
	</head>
	<body>
    <div class="wrapper">
     <div class="main-menu">
        <div class="menu font-face">
          <ul>
            <li><a href="<?php echo URL_ROOT ?>index.php">خانه</a></li>
            <li><a href="<?php echo URL_ROOT ?>help.php">راهنما</a></li>
            <li><a href="<?php echo URL_ROOT ?>privacy.php">حریم شخصی</a></li>
            <li><a href="<?php echo URL_ROOT ?>gallery.php">گالری</a></li>            
            <li><a href="<?php echo URL_ROOT ?>newOrder.php">سفارش</a></li>
            <li><a href="<?php echo URL_ROOT ?>follow.php">رهگیری</a></li>
            <!--<li><a href="<?php echo URL_ROOT ?>mPay.php">پرداخت</a></li>-->
            <li><a href="<?php echo URL_ROOT ?>contact.php">تماس با ما</a></li>
          </ul>
        </div>
      </div>   
      <div id="messages">
    	<?php
        	self::printErr(self::$error);
        	self::printSuc(self::$success);
    	?>   
    </div>
	<div class="content">