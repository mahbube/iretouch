<?php _var("subject") ?>
<script type="text/javascript">
    $(document).ready(function(){
        $("#submitForm").click(function(){
            $("#mailForm").submit(); 
        });    
    });
</script>    
<form id="mailForm" action="?act=sendMail&id=<?php echo $id ?>" method="post" enctype="multipart/form-data"> 
    <div class="step" id="step2">        
        <div class="inside-content order2">
            <label><span class="font-face">شماره فاکتور :</span><input disabled="disabled" type="text" value="<?php echo $factorNumber ?>"></label>
            <label><span class="font-face">نام مشتری :</span><input disabled="disabled" name="cusLastName" type="text" value="<?php echo $cusFirstName." ".$cusLastName ?>"></label>
            <label><span class="font-face">ایمیل مشتری :</span><input disabled="disabled" type="text" name="cusEmail" value="<?php echo $cusEmail ?>"></label>
            <label><span class="font-face">موضوع :</span><input name="subject" type="text" value="<?php echo $subject ?>"></label>
            <label><span class="font-face">فایل :</span><input name="file" id="file" type="file"></label>
            <label><span class="font-face">متن پیام :</span><textarea cols="30" rows="5" name="message" placeholder="پیام شما برای مشتری"></textarea></label>
            <a id="submitForm" class="next font-face">ارسال</a>
            <div class="clear"></div>
        </div>        
    </div>
</form>    